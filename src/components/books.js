import React from 'react';

import { SearchContext }  from './menu.js';
import BibItem from './bib-item';
import { BooksList } from './items-lists';


const Books = () => {
    return (
        <SearchContext.Consumer>
            {(searchVal) => (
                <div style={{display: 'inline-block', width: '95%', margin: '0 auto'}}>
                    {BooksList.map(book => {
                        return ((book.name.includes(searchVal) || book.author.includes(searchVal) ||
                                book.brief.includes(searchVal)) &&
                                <BibItem key={book.name + book.author} name={book.name} displayName={book.author}
                                        brief={book.brief} />);
                    })}
                </div>
            )}
        </SearchContext.Consumer>
    );
}

export default Books;