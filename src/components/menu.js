import React, { useState, createContext, Fragment } from 'react';
import { Tabs, Tab, AppBar, TextField } from '@material-ui/core';

import Books from './books';
import Movies from './movies';


export const SearchContext = createContext('');

const Menu = () => {
    const [selectedTab, setSelectedTab] = useState(0);
    const [searchVal, setSearchVal] = useState('');

    const handleChange = (e, newVal) => {
        setSelectedTab(newVal);
    }

    const handleSearch = (e) => {
        setSearchVal(e.target.value);
    }

    return (
        <Fragment>
            <AppBar position='sticky' style={{backgroundColor: '#4ca9dfc7', color: 'black'}} >
                <Tabs value={selectedTab} onChange={handleChange} >
                    <Tab id='books' label='Books' />
                    <Tab id='movies' label='Movies' />
                </Tabs>
                <TextField id='search' style={{position: 'fixed', alignSelf: 'center', marginRight: '48px'}}
                    label='Search' value={searchVal} onChange={handleSearch} />
            </AppBar>
            <SearchContext.Provider value={searchVal} >
                { selectedTab === 0 && <Books /> }
                { selectedTab === 1 && <Movies /> }
            </SearchContext.Provider>
        </Fragment>
    );
}

export default Menu;