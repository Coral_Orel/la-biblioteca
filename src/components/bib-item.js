import React, { useState } from 'react';
import { Card, CardHeader, CardMedia, CardContent, CardActions, Collapse, IconButton } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';


const BibItem = (props) => {
    const [expanded, setExpanded] = useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    }

    return (
        <Card style={{float: 'left', width: '250px', margin: '16px 8px'}}>
            <CardHeader title={props.name} subheader={props.displayName} />
            <CardActions disableSpacing>
                <IconButton style={{transform: expanded? 'rotate(180deg)' : 'rotate(0deg)', marginLeft: 'auto'}}
                    onClick={handleExpandClick} aria-expanded={expanded} aria-label='show more'>
                    <ExpandMoreIcon />
                </IconButton>
            </CardActions>
            <Collapse in={expanded} timeout='auto' unmountOnExit >
                <CardContent>
                    {props.brief}
                </CardContent>
            </Collapse>
        </Card>
    );
}

export default BibItem;