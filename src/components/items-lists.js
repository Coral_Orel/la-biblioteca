export const BooksList = [
    {
        name: '50 Shades Of Grey',
        author: 'E. L. James',
        brief: 'A little brief about fifty shades of grey'
    },
    {
        name: 'Beautiful Disaster',
        author: 'Jamie McGuire',
        brief: 'A little brief about beautiful disaster'
    },
    {
        name: 'Me Before You',
        author: 'Jojo Moyes',
        brief: 'A little brief about me before you'
    },
]

export const MoviesList = [
    {
        name: 'Grease',
        director: 'Randal Kleiser',
        brief: 'A little brief about grease'
    },
    {
        name: 'Dirty Dancing',
        director: 'Emile Ardolino',
        brief: 'A little brief about dirty dancing'
    },
    {
        name: 'The Green Mile',
        director: 'Frank Darabont',
        brief: 'A little brief about the green mile'
    },
]