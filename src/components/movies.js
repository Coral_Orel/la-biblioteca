import React from 'react';

import { SearchContext } from './menu';
import BibItem from './bib-item';
import { MoviesList } from './items-lists';


const Movies = () => {
    return (
        <SearchContext.Consumer>
            {(searchVal) => (
                <div style={{display: 'inline-block', width: '95%', margin: '0 auto'}}>
                    {MoviesList.map(movie => {
                        return ((movie.name.includes(searchVal) || movie.director.includes(searchVal) ||
                                movie.brief.includes(searchVal)) &&
                                <BibItem key={movie.name + movie.director} name={movie.name}
                                        displayName={movie.director} brief={movie.brief} />);
                    })}
                </div>
            )}
        </SearchContext.Consumer>
    );
}

export default Movies;